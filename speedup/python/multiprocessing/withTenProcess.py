import time
from multiprocessing import Process

def plusFive(v):
    for i in range(len(v)):
        v[i]= v[i]+5
    return v

def plusFive2(v, start, end):
    for i in range(start,end):
        v[i] = v[i]+5
    return v

vect = [0]*100000000

p1 = Process(target=plusFive2, args=(vect,0,20000000))
p2 = Process(target=plusFive2, args=(vect,20000000,20000000))
p3 = Process(target=plusFive2, args=(vect,40000000,20000000))
p4 = Process(target=plusFive2, args=(vect,60000000,20000000))
p5 = Process(target=plusFive2, args=(vect,80000000,20000000))
p6 = Process(target=plusFive2, args=(vect,100000000,20000000))
p7 = Process(target=plusFive2, args=(vect,120000000,20000000))
p8 = Process(target=plusFive2, args=(vect,140000000,20000000))
p9 = Process(target=plusFive2, args=(vect,160000000,20000000))
p10 = Process(target=plusFive2, args=(vect,180000000,20000000))

startTime = time.time()

p1.start()
p2.start()
p3.start()
p4.start()
p5.start()
p6.start()
p7.start()
p8.start()
p9.start()
p10.start()

p1.join()
p2.join()
p3.join()
p4.join()
p5.join()
p6.join()
p7.join()
p8.join()
p9.join()
p10.join()

endTime = time.time()

print("Finished! Time taken", endTime-startTime)


