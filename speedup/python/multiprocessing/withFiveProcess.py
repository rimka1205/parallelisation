import time
from multiprocessing import Process

def plusFive(v):
    for i in range(len(v)):
        v[i]= v[i]+5
    return v

def plusFive2(v, start, end):
    for i in range(start,end):
        v[i] = v[i]+5
    return v

vect = [0]*100000000

p1 = Process(target=plusFive2, args=(vect,0,20000000))
p2 = Process(target=plusFive2, args=(vect,20000000,20000000))
p3 = Process(target=plusFive2, args=(vect,40000000,20000000))
p4 = Process(target=plusFive2, args=(vect,60000000,20000000))
p5 = Process(target=plusFive2, args=(vect,80000000,20000000))

startTime = time.time()

p1.start()
p2.start()
p3.start()
p4.start()
p5.start()

p1.join()
p2.join()
p3.join()
p4.join()
p5.join()

endTime = time.time()

print("Finished! Time taken", endTime-startTime)


