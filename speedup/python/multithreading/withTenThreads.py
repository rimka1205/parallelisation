import time
import threading

def plusFive(v):
    for i in range(len(v)):
        v[i]= v[i]+5
    return v

def plusFive2(v, start, end):
    for i in range(start,end):
        v[i] = v[i]+5
    return v

vect = [0]*100000000


t1 = threading.Thread(target=plusFive2, args=(vect,0,20000000))
t2 = threading.Thread(target=plusFive2, args=(vect,20000000,20000000))
t3 = threading.Thread(target=plusFive2, args=(vect,40000000,20000000))
t4 = threading.Thread(target=plusFive2, args=(vect,60000000,20000000))
t5 = threading.Thread(target=plusFive2, args=(vect,80000000,20000000))
t6 = threading.Thread(target=plusFive2, args=(vect,100000000,20000000))
t7 = threading.Thread(target=plusFive2, args=(vect,120000000,20000000))
t8 = threading.Thread(target=plusFive2, args=(vect,140000000,20000000))
t9 = threading.Thread(target=plusFive2, args=(vect,160000000,20000000))
t10 = threading.Thread(target=plusFive2, args=(vect,180000000,20000000))

startTime = time.time()

t1.start()
t2.start()
t3.start()
t4.start()
t5.start()
t6.start()
t7.start()
t8.start()
t9.start()
t10.start()

t1.join()
t2.join()
t3.join()
t4.join()
t5.join()
t6.join()
t7.join()
t8.join()
t9.join()
t10.join()

endTime = time.time()

print("Finished! Time taken", endTime-startTime)


