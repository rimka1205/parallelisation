import time
import threading

def plusFive(v):
    for i in range(len(v)):
        v[i]= v[i]+5
    return v

def plusFive2(v, start, end):
    for i in range(start,end):
        v[i] = v[i]+5
    return v

vect = [0]*100000000

t1 = threading.Thread(target=plusFive2, args=(vect,0,50000000))
t2 = threading.Thread(target=plusFive2, args=(vect,50000000,50000000))

startTime = time.time()

t1.start()
t2.start()

t1.join()
t2.join()

endTime = time.time()

print("Finished! Time taken", endTime-startTime)
