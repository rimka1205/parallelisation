#include <iostream>
#include <vector>
#include <thread>

std::vector<int> addFive(std::vector<int> v){ 
	for(int i = 0; i<v.size(); i++){ 
		v[i] = v[i]+5;
	}
	return v;
}

std::vector<int> addFive2(std::vector<int> v, int start, int number){ 
	for(int i = start; i<start+number; i++){ 
		v[i] = v[i]+5;
	}
	return v;
}

int main(int argc, char **argv){ 
	std::vector<int> v (500000000);

	time_t start_time = time(NULL);
	
	std::thread t1 (addFive2, v, 0, 250000000);
	addFive2(v,250000000,250000000);

	t1.join();

	time_t end_time = time(NULL);
	std::cout << "done in " << end_time-start_time << " seconds" << std::endl;
}

