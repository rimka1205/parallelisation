#include <iostream>
#include <vector>

std::vector<int> addFive(std::vector<int> v){ 
	for(int i = 0; i<v.size(); i++){ 
		v[i] = v[i]+5;
	}
	return v;
}

int main(int argc, char **argv){ 
	std::vector<int> v (500000000);

	time_t start_time = time(NULL);
	v = addFive(v);
	time_t end_time = time(NULL);
	std::cout << "done in " << end_time-start_time << " seconds" << std::endl;
}
