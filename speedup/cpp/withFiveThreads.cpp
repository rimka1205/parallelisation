#include <iostream>
#include <vector>
#include <thread>

std::vector<int> addFive(std::vector<int> v){ 
	for(int i = 0; i<v.size(); i++){ 
		v[i] = v[i]+5;
	}
	return v;
}

std::vector<int> addFive2(std::vector<int> v, int start, int number){ 
	for(int i = start; i<start+number; i++){ 
		v[i] = v[i]+5;
	}
	return v;
}

int main(int argc, char **argv){ 
	std::vector<int> v (500000000);

	time_t start_time = time(NULL);
	
	std::thread t1 (addFive2, v, 0, 100000000);
	std::thread t2 (addFive2, v, 100000000, 100000000);
	std::thread t3 (addFive2, v, 200000000, 100000000);
	std::thread t4 (addFive2, v, 300000000, 100000000);
	addFive2(v,400000000,100000000);

	t1.join();
	t2.join();
	t3.join();
	t4.join();

	time_t end_time = time(NULL);
	std::cout << "done in " << end_time-start_time << " seconds" << std::endl;
}

