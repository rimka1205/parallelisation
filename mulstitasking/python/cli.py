import os, sys, time, vlc
from playsound import playsound
from threading import Thread

my_song = None

def play(audio):
    global my_song
    my_song = vlc.MediaPlayer(audio)
    my_song.play()

def fnctimer(seconds):
    time.sleep(seconds)
    print("fini")

while True:
    inp = input()

    if inp == "ls" :
        print(os.listdir())
    if inp == "calc" :
        print("Veuillez saisir votre calcul")
        calcul = input()
        print(eval(calcul))
    if inp == "play" :
        print("Veuillez saisir le nom de votre fichier audio")
        audio = input()
        t= Thread(target= play, args=(audio, ))
        t.start()
    if inp == "stop":
        my_song.stop()
    if inp == "timer":
        print("Veuillez saisir le nombre de secondes pour le timer")
        seconds = int(input())
        timer = Thread(target= fnctimer, args=(seconds, ))
        timer.start()
