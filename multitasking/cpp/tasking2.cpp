#include <iostream>
#include <vector>
#include <thread>
#include <unistd.h>
#include <stdio.h>
#include <string.h>

void work(){ 
  sleep(10);
  puts ("\nWork done!");
}

int main ()
{
  char key[] = "work";
  char buffer[80];
  while(1){ 
     printf ("prompt>> ");
     fflush (stdout);
     scanf ("%79s",buffer);
	 if(strcmp (key,buffer) == 0){ 
		 std::thread t1 (work);
		 t1.detach();
	 }
	 else{ 
		  puts ("You puts: ");
		  puts (buffer);
	 }
  
  }
  return 0;
}
